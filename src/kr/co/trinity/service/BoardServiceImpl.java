package kr.co.trinity.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import kr.co.trinity.dao.BoardDAO;
import kr.co.trinity.dao.BoardDAOIF;
import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.vo.Board;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoardServiceImpl implements BoardServiceIF{
	
	@Autowired
	BoardDAOIF db;
	PagingOperator po=new PagingOperator();
	
	@Override
	public void insert(Board b) {
		// TODO Auto-generated method stub
		db.insert(b);
	}

	@Override
	public void update(Board b) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		db.update(b);
	}

	@Override
	public void delete(int boardNum) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		db.delete(boardNum);
	}

	@Override
	public Board getBoard(int boardNum) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return db.getBoard(boardNum);
	}

	@Override
	public ArrayList<Board> getAllBoard() {
		// TODO Auto-generated method stub
		return db.getAllBoard();
	}

	@Override
	public ArrayList<Board> getBoardFindByTitle(String title) {
		// TODO Auto-generated method stub
		return db.getBoardFindByTitle(title);
	}

	@Override
	public ArrayList<Board> getBoardFindByWriter(String writer) {
		// TODO Auto-generated method stub
		return db.getBoardFindByWriter(writer);
	}

	@Override
	public int getTotalCount() {
		// TODO Auto-generated method stub
		return db.getTotalCount();
	}

	@Override
	public ArrayList<Board> getAllBoard(int pageSize, int pageNumber) {
		// TODO Auto-generated method stub
		return db.getAllBoard(pageSize, pageNumber);
	}

	@Override
	public PagingOperator paging(int _pageNumber, int _pageSize, int totalCount, String url) {
		// TODO Auto-generated method stub
		return po.excute(_pageNumber, _pageSize, totalCount, url);
	}

	@Override
	public void fileDownload(HttpServletResponse response, String filePath) throws Exception {
		File downfile = new File(filePath);
		if(!downfile.exists()){
			throw new FileNotFoundException();
		}
		ServletOutputStream outStream = null;
		FileInputStream inputStream = null;
		
		try {
	        outStream = response.getOutputStream();
	        inputStream = new FileInputStream(downfile);               
	 
	        //Setting Resqponse Header
	        response.setContentType("application/octet-stream");
	        response.setHeader("Content-Disposition",                     
	                           "attachment;filename=\""+downfile.getName()+"\"");
	              
	        //Writing InputStream to OutputStream
	        byte[] outByte = new byte[4096];
	        while(inputStream.read(outByte, 0, 4096) != -1)
	        {
	          outStream.write(outByte, 0, 4096);
	        }
		} catch (Exception e) {
		        throw new IOException();
		} finally {
		    inputStream.close();
		    outStream.flush();
		    outStream.close();
		}
		
	}

	@Override
	public String md5Cipher(String password) throws NoSuchAlgorithmException {
		String MD5 = ""; 
		
		MessageDigest md = MessageDigest.getInstance("MD5"); 
		md.update(password.getBytes()); 
		byte byteData[] = md.digest();
		MD5=Hex.encodeHexString(byteData);
		System.out.println("����: "+password+ "   MD5: "+MD5);
		
		return MD5;
	}

	public ArrayList<Board> getBoardFindByWriter(int pageNumber, int pageSize, String keyWord) {
		// TODO Auto-generated method stub
		
		return db.getBoardFindByWriter(pageNumber, pageSize, keyWord);
	}

	
}

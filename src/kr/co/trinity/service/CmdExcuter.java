package kr.co.trinity.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CmdExcuter {
	String result = new String();
	Process process = null;
	public String excute(String cmd){
		String cmdString = "cmd.exe /c ";
		cmdString += cmd;
		try{
			
			process = Runtime.getRuntime().exec(cmd);
			/*
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr); 
			
			String line = null;
			while((line = br.readLine()) != null)
			{
				result = result + line + "<br>";
				//System.out.println(result);
			}*/
			process.waitFor();
			
			if(cmdString.equals("cmd.exe /c exp userid=scott/tiger file='c:\\DBbak\\test.dmp'"))
				result = "DB 백업 완료";
			
		}catch(Exception e){
			System.err.println(e);			
		}finally{
			if(process!=null)
			process.destroy();
		}
		
		
		
		return result;
	}
}
/*
class StreamGobbler extends Thread
{

    InputStream is;
    String type;
    String result; 
    
    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
    }

    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while((line = br.readLine()) != null){
                System.out.println(type + ">" + line);
            }
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }
}
*/
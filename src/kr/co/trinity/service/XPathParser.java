package kr.co.trinity.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.namespace.QName;
import javax.xml.parsers.*;
import javax.xml.xpath.*;

import kr.co.trinity.vo.Product;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class XPathParser {
	NodeList nodes = null;	
	ArrayList<Product> pdList = new ArrayList<Product>();
	public ArrayList<Product> excute(String dir, String keyWord, String flag) throws FileNotFoundException, XPathExpressionException{
		File d = new File(dir);
		XPathFactory factory = XPathFactory.newInstance();
		XPath xPath = factory.newXPath();
		InputSource inputSource = new InputSource(new FileInputStream(d));
		
		
		String expression = "/trinity/TSstore/product[itemID/text()= '" + keyWord + "' and hidden/text() = '" + flag + "']";
		
		nodes = (NodeList) xPath.evaluate(expression, inputSource, XPathConstants.NODESET);
		
		if(nodes != null && nodes.getLength() > 0 /*&& checkValid(keyWord)&& checkValid(flag)*/) {
			for (int count = 0; count < nodes.getLength(); count++) {
				
				Element el = (Element)nodes.item(count);
				
				Element itemId = (Element)el.getElementsByTagName("itemID").item(0);
				Element price = (Element)el.getElementsByTagName("price").item(0);
				Element descr = (Element)el.getElementsByTagName("description").item(0);
				
				String resultOfitemId = itemId.getFirstChild().getNodeValue();
				int resultOfprice = Integer.parseInt(price.getFirstChild().getNodeValue());
				String resultOfdescr = descr.getFirstChild().getNodeValue();
				
				Product pd = null;
				pd = new Product(resultOfitemId, resultOfprice, resultOfdescr);
				
				pdList.add(pd);
			}
		}
		return pdList;
	}
	
	public ArrayList<Product> excute2(String dir, String keyWord, String flag) throws FileNotFoundException, XPathExpressionException{
		String expression = "/trinity/TSstore/product[itemID/text()= $keyWord and hidden/text() = $flag]";
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc = null;
		
		try {
			builder = domFactory.newDocumentBuilder();
			doc = builder.parse(dir);
			
			XPath xPath = XPathFactory.newInstance().newXPath();
			SimpleVariableResolver resolver = new SimpleVariableResolver();
			resolver.addVariable(new QName("keyWord"), keyWord);
			resolver.addVariable(new QName("flag"), flag);
			xPath.setXPathVariableResolver(resolver);

			XPathExpression expr = xPath.compile(expression);
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			
			NodeList nodes = (NodeList) result;
			if(nodes != null && nodes.getLength() > 0) {
				for (int count = 0; count < nodes.getLength(); count++) {
					
					Element el = (Element)nodes.item(count);
					
					Element itemId = (Element)el.getElementsByTagName("itemID").item(0);
					Element price = (Element)el.getElementsByTagName("price").item(0);
					Element descr = (Element)el.getElementsByTagName("description").item(0);
					
					String resultOfitemId = itemId.getFirstChild().getNodeValue();
					int resultOfprice = Integer.parseInt(price.getFirstChild().getNodeValue());
					String resultOfdescr = descr.getFirstChild().getNodeValue();
					
					Product pd = null;
					pd = new Product(resultOfitemId, resultOfprice, resultOfdescr);
					
					pdList.add(pd);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pdList;
	}
	
	/*
	//XPathParser.java 클래스 내에 checkValid 메소드를 추가하여 
	//쿼리가 조작될 수 있는 문자를 필터링 하여 제거한다.
 
	public boolean checkValid(String value) 
	{
		boolean flag = true;
		
		if(value != null && !"".equals(value))
		{
			String checkList = "()='[]:,/ ";
			String decodeValue = null;
			try {
				decodeValue = URLDecoder.decode(value, Charset.defaultCharset().name());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			for(char c : decodeValue.toCharArray()){
				if(checkList.indexOf(c) != -1){
					System.out.println(c);
					flag = false;
					break;
				}
			}
		}
		return flag;
	}*/
	
	public Product buildProduct(Element ele) {
		Product pd = null;
		String  itemId = getTextValue(ele, "itemID");
		int 	price  = getIntValue(ele, "price");
		String  descr  = getTextValue(ele, "description");
		String  hidden = getTextValue(ele, "hidden");
		
		System.out.println(hidden);
		
		if(hidden.equals("945231"))
		{
			pd = new Product(itemId, price, descr);
			return pd;
		}
		return pd;
	}
	
	public String getTextValue(Element ele, String tagName) {
		String text = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			text = el.getFirstChild().getNodeValue();
		}
		return text;
	}
	
	public int getIntValue(Element ele, String tagName) {
		return Integer.parseInt(getTextValue(ele, tagName));
	}
}

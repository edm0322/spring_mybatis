package kr.co.trinity.service;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.xpath.XPathExpressionException;

import kr.co.trinity.exception.DulplicateException;
import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.vo.Customer;

public interface CustomerServiceIF {
	public abstract void insert(Customer customer) throws DulplicateException;
	public abstract void update(Customer customer) throws RecordNotFoundException;
	public abstract void delete(String userId) throws RecordNotFoundException;
	public abstract Customer getCustomer(String userNum) throws RecordNotFoundException;
	public abstract ArrayList<Customer> getAllCustomer();
	public abstract boolean loginCheck(String userId, String password) throws Exception;
	public abstract boolean loginCheck(String userId) throws Exception;
	public abstract boolean adminCheck(String userId, String password) throws Exception;
	public abstract String CmdExcuter(String cmd);
}

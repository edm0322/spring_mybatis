package kr.co.trinity.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.vo.Board;

public interface BoardServiceIF {
	public abstract void insert(Board b);
	public abstract void update(Board b) throws RecordNotFoundException;
	public abstract void delete(int boardNum) throws RecordNotFoundException;
	public abstract Board getBoard(int boardNum) throws RecordNotFoundException;
	public abstract ArrayList<Board> getAllBoard();
	public abstract ArrayList<Board> getBoardFindByTitle(String title);
	public abstract ArrayList<Board> getBoardFindByWriter(String writer);
	public abstract int getTotalCount();
	public abstract ArrayList<Board> getAllBoard(int pageSize, int pageNumber);
	public abstract PagingOperator paging(int _pageNumber, int _pageSize, int totalCount, String url);
	public abstract void fileDownload(HttpServletResponse response, String filePath) throws Exception;
	public abstract String md5Cipher(String password) throws NoSuchAlgorithmException;
}

package kr.co.trinity.service;

public class PagingOperator {
	private int totalCount = 0 ; //珥??덉퐫??嫄댁닔
	private int totalPage = 0 ; //?꾩껜 ?섏씠吏 ??
	
	private int pageNumber = 0 ; //蹂댁뿬以??섏씠吏 ?섎쾭(?쒗쁽 媛?ν븳 ?섏씠吏??1遺??totalPage源뚯??대떎.)
	private int pageSize = 0 ; //???섏씠吏??蹂댁뿬以?嫄댁닔
	private int beginRow = 0 ; //?꾩옱 ?섏씠吏???쒖옉 ??
	private int endRow = 0 ; //?꾩옱 ?섏씠吏??????
	
	private int pageCount = 10 ; //蹂댁뿬以??섏씠吏 留곹겕 ??
	private int beginPage = 0 ; //?섏씠吏?泥섎━ ?쒖옉 ?섏씠吏 踰덊샇
	private int endPage = 0 ; //?섏씠吏?泥섎━ ???섏씠吏 踰덊샇
	
	private String url = "" ; 
	private String pagingHtml = "";//?섎떒???レ옄 ?섏씠吏 留곹겕
	private String pagingStatus = ""; //?곷떒 ?곗륫???꾩옱 ?섏씠吏 ?꾩튂 ?쒖떆
	
	//寃?됱쓣 ?꾪븳 蹂??異붽?
	private String mode = "" ; //寃??紐⑤뱶(?묒꽦?? 湲?쒕ぉ, ?꾩껜 寃?됱? all) ?깅벑
	private String keyword = "" ; //寃?됲븷 ?⑥뼱  
	

	public PagingOperator excute(int _pageNumber, int _pageSize, int totalCount, String url) {
		this.totalCount = totalCount ; 
	
		
		this.totalPage = (int)Math.ceil((double)this.totalCount / this.pageSize) ;
		
		this.beginRow = ( this.pageNumber - 1 )  * this.pageSize  + 1 ;
		//this.endRow = Math.min( this.pageNumber * this.pageSize, this.totalCount )  ;
		this.endRow =  this.pageNumber * this.pageSize ;
		if( this.endRow > this.totalCount ){
			this.endRow = this.totalCount  ;
		}
		
		this.beginPage = ( this.pageNumber - 1 ) / this.pageCount * this.pageCount + 1  ;
		this.endPage = this.beginPage + this.pageCount - 1 ;
		
		if( this.endPage > this.totalPage ){
			 this.endPage = this.totalPage ;
		}
		
		this.url = url ;
		
		this.pagingHtml = getPagingHtml(url) ;
		this.pagingStatus = "珥?" + this.totalCount + "嫄?" + this.pageNumber + "/" + this.totalPage + "]" ; 
		
		return this;
		//displayInformation() ;
	}
	
	private String getPagingHtml( String url ){ 
		String result = "" ;
		
		if (this.beginPage != 1) { //?욎そ
			result += "&nbsp;<a href='" + url
					+ "&pageNumber=" + ( 1 ) + "&pageSize=" + this.pageSize + "'>留?泥섏쓬</a>&nbsp;" ;
			result += "&nbsp;<a href='" + url
					+ "&pageNumber=" + (this.beginPage - 1 ) + "&pageSize=" + this.pageSize + "'>?댁쟾</a>&nbsp;" ;
		}
		
		for (int i = this.beginPage; i <= this.endPage ; i++) {
			if ( i == this.pageNumber ) {
				result += "&nbsp;<font color='red'>" + i + "</font>&nbsp;"	;
						
			} else {
				result += "&nbsp;<a href='" + url 
						+ "&pageNumber=" + i + "&pageSize=" + this.pageSize + "' onclick='paging(" + i + ")''>" + i + "</a>&nbsp;" ;
			}
		}
		
		if ( this.endPage != this.totalPage) {
			result += "&nbsp;<a href='" + url
					+ "&pageNumber=" + (this.endPage + 1 ) + "&pageSize=" + this.pageSize + "'>?ㅼ쓬</a>&nbsp;" ;
			result += "&nbsp;<a href='" + url 
					+ "&pageNumber=" + (this.totalPage ) + "&pageSize=" + this.pageSize + "'>留???/a>&nbsp;" ;
		}		
		
		return result ;
	}	

	 
	
	@Override
	public String toString() {
		return "Paging [totalCount=" + totalCount + ", totalPage=" + totalPage
				+ ", pageNumber=" + pageNumber + ", pageSize=" + pageSize
				+ ", beginRow=" + beginRow + ", endRow=" + endRow
				+ ", pageCount=" + pageCount
				+ ", beginPage=" + beginPage + ", endPage=" + endPage
				+ ", url=" + url 
				+ ", pagingStatus=" + pagingStatus + ", mode=" + mode
				+ ", keyword=" + keyword + "]/n"
				+ ", pagingHtml=" + pagingHtml;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getBeginRow() {
		return beginRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getBeginPage() {
		return beginPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public String getUrl() {
		return url;
	}

	public String getPagingHtml() {
		return pagingHtml;
	}

	public String getPagingStatus() {
		return pagingStatus;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMode() {return mode;}
	public String getKeyword() { return keyword; 	}	 
}

package kr.co.trinity.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import kr.co.trinity.vo.Product;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class StoreXMLLogic {
	
	ArrayList<Product> pdList = new ArrayList<Product>();
	
	DocumentBuilderFactory builderFact = DocumentBuilderFactory.newInstance();
	
	public ArrayList<Product> getXML(String dir) throws ParserConfigurationException, SAXException, IOException {
		File file = new File(dir);
		DocumentBuilder builder = builderFact.newDocumentBuilder();
		Document doc = builder.parse(file);
		
		pdList.removeAll(pdList);
		pdList = nodeToArrayList(doc);
		
		return pdList;
	}
	
	public ArrayList<Product> nodeToArrayList(Document doc) {
		
		// root element
		Element ele = doc.getDocumentElement();
		NodeList nodeLists = ele.getElementsByTagName("product");
		
		if(nodeLists != null && nodeLists.getLength() > 0) {
			for (int count = 0; count < nodeLists.getLength(); count++) {
				
				Element el = (Element)nodeLists.item(count);
				Product pd = buildProduct(el);
				
				if(pd !=null)
				pdList.add(pd);
			}
		}
		return pdList;
	}
	
	public Product buildProduct(Element ele) {
		Product pd = null;
		String  itemId = getTextValue(ele, "itemID");
		int 	price  = getIntValue(ele, "price");
		String  descr  = getTextValue(ele, "description");
		String  hidden = getTextValue(ele, "hidden");

		if(hidden.equals("945231"))
		{
			pd = new Product(itemId, price, descr);
			return pd;
		}
		else
			return pd;
	}
	
	public String getTextValue(Element ele, String tagName) {
		String text = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			text = el.getFirstChild().getNodeValue();
		}
		return text;
	}
	
	public int getIntValue(Element ele, String tagName) {
		return Integer.parseInt(getTextValue(ele, tagName));
	}
}

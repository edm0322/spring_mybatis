package kr.co.trinity.service;

import java.util.ArrayList;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.trinity.dao.CustomerDAO;
import kr.co.trinity.dao.CustomerDAOIF;
import kr.co.trinity.exception.DulplicateException;
import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.vo.Customer;

@Service
public class CustomerServiceImpl implements CustomerServiceIF {
	@Autowired
	CustomerDAOIF db;
	
	XPathParser xp = new XPathParser();
	CmdExcuter ce = new CmdExcuter();
	
	@Override
	public void delete(String id) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		db.delete(id);
	}

	@Override
	public void insert(Customer customer) throws DulplicateException {
		// TODO Auto-generated method stub
		db.insert(customer);
	}

	@Override
	public void update(Customer customer) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		db.update(customer);
	}

	@Override
	public Customer getCustomer(String userNum) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return db.getCustomer(userNum);
	}
	
	@Override
	public ArrayList<Customer> getAllCustomer() {
		// TODO Auto-generated method stub
		return db.getAllCustomer();
	}
	
	@Override
	public boolean loginCheck(String userId, String password) throws Exception {
		return db.loginCheck(userId, password);
	}
	
	@Override
	public boolean loginCheck(String userId) throws Exception {
		// TODO Auto-generated method stub
		return db.loginCheck(userId);
	}
	
	@Override
	public boolean adminCheck(String userId, String password) throws Exception {
		// TODO Auto-generated method stub
		return db.adminCheck(userId, password);
	}
	

	@Override
	public String CmdExcuter(String cmd) {
		// TODO Auto-generated method stub
		return ce.excute(cmd);
	}
	
	public String getUserNum(String userId) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		return db.getUserNum(userId);
	}

}

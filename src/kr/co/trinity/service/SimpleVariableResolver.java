package kr.co.trinity.service;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathVariableResolver;

public class SimpleVariableResolver implements XPathVariableResolver{
	private static final Map<QName, Object> vars = new HashMap<QName, Object>();
	
	public void addVariable(QName name, Object value){
		vars.put(name, value);
	}
	
	public Object resolveVariable(QName name){
		return vars.get(name);
	}
}

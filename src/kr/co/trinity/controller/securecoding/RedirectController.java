package kr.co.trinity.controller.securecoding;

import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectController{

	@RequestMapping()
	public String redirect(String site, HttpServletResponse response)throws Exception{
		InetAddress addr = null;
		boolean trusted = false;
		
	
		if(site.equals("http://cafe.naver.com/sec") || site.equals("https://www.facebook.com/trinitysoft"))
		{
			response.sendRedirect(site);
			return "comitted";
		}
		else 
		{
			// dns lookup
			addr = InetAddress.getByName("kisa.or.kr");
		}
			if(addr.getHostName().equals("kisa.or.kr"))
		{
			trusted = true;
		}
		
		if(trusted)
		{
			response.sendRedirect(site);
			return "comitted";
		}
		else
		{
			return "index.jsp?content=login";
		}
	}
	

}

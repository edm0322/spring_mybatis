package kr.co.trinity.controller.securecoding;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RandomgeneratorController {

	@RequestMapping("/randomgenerator")
	public String ramdom(String math, String secure,
			             HttpServletRequest request) throws NoSuchAlgorithmException{
	
		String value = null;
		ArrayList<String> result = new ArrayList<String>();
		SecureRandom rand = null;
		
		if(math != null)
		{
			for(int i = 0; i < 5; i++)
			{
				value = "" + Math.random();
				result.add(value);
			}
		}
		else if(secure != null)
		{
			for(int i =0; i < 5; i++)
			{
				rand = SecureRandom.getInstance("SHA1PRNG");
				value = "" + rand.nextDouble();
				result.add(value);
			}
		}
		else
		{
			result = null;
		}
		
		request.setAttribute("result", result);
		
		return "index.jsp?content=securecoding/randomgenerator";
	}
	
}

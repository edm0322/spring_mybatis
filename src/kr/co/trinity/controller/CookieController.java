package kr.co.trinity.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CookieController {

	@RequestMapping("/xss")
	public String home(HttpServletRequest request,HttpSession session){
		Cookie[] cookie = request.getCookies();
		
		String fileName = "C:\\Cookie\\test.txt";
		
		File file = new File(fileName);
		
		try {
			FileWriter fw = new FileWriter(file, true);
			
			for(int i =0; i<cookie.length; i++)
			{
				fw.write(cookie[i].getName()+ " ");
				fw.write(cookie[i].getValue()+ " ");
				fw.flush();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		if(session != null ) session.invalidate();
		
		return "index.jsp?content=login";	
	}

}
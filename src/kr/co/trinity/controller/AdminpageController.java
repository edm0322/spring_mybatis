package kr.co.trinity.controller;

import javax.servlet.http.HttpServletRequest;

import kr.co.trinity.service.CustomerServiceImpl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminpageController {

	CustomerServiceImpl cs = new CustomerServiceImpl();
	
	@RequestMapping(value="/admin")
	public String home(String userId){
		return "index.jsp?content=administrator/adminpage";
	}
	
	@RequestMapping(value="/admin", params="action=backup")
	public String admin(String userId, HttpServletRequest request){
		
		String cmd = request.getParameter("cmd");
		String result = null;
		result = cs.CmdExcuter(cmd);
//		String validate = "exp userid=scott/tiger file='c:\\DBbak\\test.dmp'";
		
//		if(validate.equals(cmd))
//		{
//			result = cs.CmdExcuter(cmd);
//		}
//		else
//		{
//			result = "허용되지 않은 명령어 입니다.";
//		}
			
		request.setAttribute("result", result);
		
		return "index.jsp?content=administrator/adminpage";
	}
}

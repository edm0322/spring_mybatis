package kr.co.trinity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.trinity.exception.DulplicateException;
import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.service.CustomerServiceImpl;
import kr.co.trinity.vo.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomerController  {
	@Autowired
	CustomerServiceImpl cs;

	@RequestMapping(value="/customer", params="action=input")
	public String input() {
		 //view select
		return "index.jsp?content=customer";
	}
	
	@RequestMapping(value="/customer", params="action=modify")
	public String modify(String userNum, HttpServletRequest request) {
		String nextPage=null;
		
		try {
			Customer customer = cs.getCustomer(userNum);

			request.setAttribute("customer", customer);
		
			nextPage="index.jsp?content=customerModify";
		} catch (RecordNotFoundException e) {
			nextPage="index.jsp?content=error";
		}
		return nextPage;
	}
	@RequestMapping(value="/customer", params="action=save")
	public String save(	String userId, 
			 			String password,
			 			String userName,
			 			int age,
			 			String email,
			           HttpServletRequest request) {
		String nextPage=null;
		String userNum = (""+(Math.random()));
			
		String[] temp = userNum.split("\\.");
		userNum = temp[1];
	
		//business method 
		try {
			cs.insert(new Customer(userId, password, userName, age, email,userNum));
			
			request.setAttribute("message",userId+"님 환영합니다.");
			nextPage = "index.jsp?content=success";
		} catch (DulplicateException e) {
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=error";
		}
	
		return nextPage;
	}	
	
	@RequestMapping(value="/customer", params="action=update")
	public String update(	String userId, 
							String userNum,
			 			String password,
			 			String userName,
			 			int age,
			 			String email,
			           HttpServletRequest request) {
		String nextPage=null;
		try {
			cs.update(new Customer(userId, password, userName, age, email, userNum));
			
			request.setAttribute("message",userId+"정보가 수정되었습니다.");
			nextPage = "index.jsp?content=result";
		} catch (RecordNotFoundException e) {
			//,view select
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=error";
		}	
		
		return nextPage;
	}
	@RequestMapping(value="customer", params="action=idcheck")
	public String update(String userId, HttpServletRequest request) throws Exception{
		String nextPage = null;
		
		boolean idChk  = cs.loginCheck(userId);
		String result = "";
		
		if(idChk)
			result = "true";
		else
			result = "false" ;
		
		request.setAttribute("userId", userId);
		request.setAttribute("result", result);
		nextPage = "index.jsp?content=idcheck";
		
	return nextPage;
}
}

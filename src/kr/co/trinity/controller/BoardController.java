package kr.co.trinity.controller;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.service.BoardServiceImpl;
import kr.co.trinity.vo.Board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.nhncorp.lucy.security.xss.XssFilter;

@Controller
public class BoardController  {
	
	@Autowired
	BoardServiceImpl bs;
	

	
	@RequestMapping(value="/board", params="action=boardInput")
	public String input(String mode, HttpServletRequest request){
		request.setAttribute("mode", mode);
		return "index.jsp?content=board/boardInput";
	}
	
	@RequestMapping(value="/board", params="action=boardDelete")
	public String delete(int boardNum,HttpServletRequest request) {
		String nextPage=null;
		try{
			bs.delete(boardNum);
			nextPage="index.jsp?content=result";
		} catch (RecordNotFoundException e) {
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=result";
		}
		
		return nextPage;
	}
	
	@RequestMapping(value="/board", params="action=boardUpdate")
	public String update(int boardNum,String title, String writer, String contents, String fileName, String password,
			              HttpServletRequest request) {
		String nextPage=null;
		try {
			if(password !=null) // 패스워드가 있는 비밀 글인 경우, 비밀번호를 암호화 한다.
			password = bs.md5Cipher(password);
			
			bs.update(new Board(boardNum, title, writer, contents, fileName, password));
			request.setAttribute("message",boardNum+"번 글이 수정되었습니다.");
			nextPage="redirect:board?action=boardList";
		} catch (RecordNotFoundException e) {
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=result";
		} catch (NoSuchAlgorithmException e) {
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=result";
		}
		
		return nextPage;
	}
	
	@RequestMapping(value="/board", params="action=boardViewSecret")
	public String viewSecret(int boardNum,String password,
			HttpServletRequest request) {
	
		String nextPage=null;
		try {
			Board board = bs.getBoard(boardNum);
			password = bs.md5Cipher(password);
			
			String test = board.getContents().replaceAll("\n", "<br>");
			board.setContents(test);
			request.setAttribute("board", board);
			
			if(password.equals(board.getPassword()))
			{
				try {
			
					Board board1 = bs.getBoard(boardNum);
					String test1 = board1.getContents().replaceAll("\n", "<br>");
					board1.setContents(test1);
					request.setAttribute("board", board1);
					nextPage="index.jsp?content=board/boardView";
				} catch (RecordNotFoundException e) {
					request.setAttribute("message", e.getMessage());
					nextPage="index.jsp?content=result";
				}
			}
			else
			{
				nextPage="redirect:board?action=boardList"; 
			}
			
		} catch (RecordNotFoundException e) {
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=result";
		}catch (NoSuchAlgorithmException e1) {
			request.setAttribute("message", e1.getMessage());
			nextPage="index.jsp?content=result";
		}
		return nextPage;
	
	}
	
	
	@RequestMapping(value="/board", params="action=boardModify")
	public String modify(int boardNum,HttpServletRequest request) {
	
		String nextPage=null;
		try {
			Board board = bs.getBoard(boardNum);
			request.setAttribute("board", board);
			nextPage="index.jsp?content=board/boardModify";
		} catch (RecordNotFoundException e) {
			request.setAttribute("message", e.getMessage());
			nextPage="index.jsp?content=result";
		}	
		return nextPage;
	
	}
    /////////////////////  MultiPart Parameter Check
	@RequestMapping(value="/board", params="action=boardSave")
	public String save(@RequestParam("fileName") MultipartFile multipartFile,
			           HttpServletRequest request) throws IllegalStateException, IOException {
	String nextPage=null;
	//Multipart File Upload 구현	
	String fileName = multipartFile.getOriginalFilename();
	long size = multipartFile.getSize();
	String contentType = multipartFile.getContentType();
	
	System.out.println(request.getServletContext().getRealPath("upload"));
	System.out.println(fileName);
	
	if(!fileName.isEmpty())
	multipartFile.transferTo(new File(request.getServletContext().getRealPath("upload")+"/"+fileName));
    
	String title=request.getParameter("title");
    String password=request.getParameter("password");
    String writer = request.getParameter("writer");
    String contents=request.getParameter("contents");
    
	if(password == null)
		bs.insert(new Board(title, writer, contents, fileName));
	
	else // board password encrypt
	{
		try {
			password = bs.md5Cipher(password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bs.insert(new Board(title, writer, contents,fileName, password));
	}
	
	request.setAttribute("filePath", request.getServletContext().getRealPath("upload"));
	
	if(password != null)
		request.setAttribute("message", password);
	
	else
		request.setAttribute("message", "글이 등록되었습니다.");
	
	nextPage="index.jsp?content=result";
	return nextPage;
   }	
	
	/////////////////////  nextPage Check
	@RequestMapping(value="/board", params="action=fileDownload")
	public String fileDownload(String fileName,
			                   HttpServletRequest request, HttpServletResponse response) {
	
		String nextPage=null;
		String fileFolder = request.getSession().getServletContext().getRealPath("upload");
		String filePath = fileFolder + "\\" + fileName;
		
		try {
			bs.fileDownload(response, filePath);
			nextPage ="comitted";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return nextPage;
	
	}
	
	
	
	@RequestMapping(value="/board", params="action=boardView")
	public String view(int boardNum, String mode, HttpServletRequest request) {
		String nextPage=null;
		if(mode.equals("private")){
			request.setAttribute("boardNum", boardNum);
			nextPage = "index.jsp?content=inputPassword";
		}
		else{
			try {
				//XssFilter filter = XssFilter.getInstance("lucy-xss-superset.xml");
				Board board = bs.getBoard(boardNum);
				String temp = board.getContents().replaceAll("\n", "<br>");
				
				//temp = filter.doFilter(temp);
				board.setContents(temp);
				
				request.setAttribute("board", board);
				nextPage="index.jsp?content=board/boardView";
			} catch (RecordNotFoundException e) {
				request.setAttribute("message", e.getMessage());
				nextPage="index.jsp?content=result";
			}
		}
		
		return nextPage;
	}

	@RequestMapping(value="/board", params="action=boardList")
	public String list(HttpServletRequest request)  {
		ArrayList<Board> boardList = null;
		String nextPage = null;
		boardList =bs.getAllBoard();
		request.setAttribute("boardList", boardList);
		
		nextPage="index.jsp?content=board/boardList";
		
		return nextPage;
	}

	@RequestMapping(value="/board", params="action=boardSearch")
	public String search(@RequestParam("keyField") String keyField,
							@RequestParam("keyWord") String keyWord,
										HttpServletRequest request)  {
		
		ArrayList<Board> boardList = null;
		String nextPage = null;
	
		if(keyField.equals("title")){ // 제목으로 검색
			boardList = bs.getBoardFindByTitle(keyWord);
		}else if(keyField.equals("writer")){ // 작성자로 검색
			boardList = bs.getBoardFindByWriter(keyWord);	
		}else{
			boardList =bs.getAllBoard();
		}
		
		request.setAttribute("boardList", boardList);
		
		nextPage="index.jsp?content=board/boardList";
		
		return nextPage;
	}
}

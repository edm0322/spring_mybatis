package kr.co.trinity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CompanyController{
	@RequestMapping(value="/company", params="action=company")
	public String company(int number){
		String nextPage=null;
		if(number==1)
			nextPage= "index.jsp?content=company/company_01";
		else if(number==2)
			nextPage="index.jsp?content=company/company_02";
		else if(number==3)
			nextPage="index.jsp?content=company/company_03";
		else if(number==4)
			nextPage="index.jsp?content=company/company_04";
		else
			nextPage="index.jsp?content=company/company_05";		
		
		return nextPage;
	}
	
	@RequestMapping(value="/company", params="action=solution")

	public String solution(int number) {
		String nextPage=null;
		if(number==1)
			nextPage="index.jsp?content=solution/solution01";
		else if(number==21 || number==2)
			nextPage="index.jsp?content=solution/solution02_1";
		else if(number==22)
			nextPage="index.jsp?content=solution/solution02_2";
		else if(number==23)
			nextPage="index.jsp?content=solution/solution02_3";
		else if(number==24)
			nextPage="index.jsp?content=solution/solution02_4";
		else
			nextPage="index.jsp?content=solution/solution03";
		
		return nextPage;
	}
}
package kr.co.trinity.controller.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.trinity.service.CustomerServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Controller
public class AdministratorController{
    // 관리자 로그인 페이지를 따로 만들었던 부분. 
	// 현재는 관리자로 로그인할 경우 관리자 기능 메뉴가 추가되는 방식
	@Autowired
	CustomerServiceImpl cs;
	
	@RequestMapping(value="/admin", params="action=administrator")
	public String administrator() {
		return "index.jsp?content=administrator/administrator";
	}
	
	@RequestMapping(value="admin", params="action=adminlogin")
	public String adminlogin(String adminid, String adminpassword,
			                 HttpServletRequest request){
		NodeList nodes = null;
		String nextPage=null;
		/*ArrayList<String> AdminList = new ArrayList<String>();
			
			
			String dir = request.getSession().getServletContext().getRealPath("xml/data.xml");
			
			if (nodes.getLength() == 1)
			{
				Node node = nodes.item(0);
				String[] arrTokens = node.getTextContent().split("[\\t\\s\\n]+");
				
				AdminList.add(arrTokens[1]);
				AdminList.add(arrTokens[2]);
				nextPage = "index.jsp?content=administrator/adminpage.jsp";
			}
			
			else if (nodes.getLength() > 1)
			{
				for (int i = 0; i < nodes.getLength(); i++)
				{
					Node node = nodes.item(i);
					String[] arrTokens = node.getTextContent().split("[\\t\\s\\n]+");
					
					AdminList.add(arrTokens[1]);
					AdminList.add(arrTokens[2]);
				}
				
				nextPage = "index.jsp?content=administrator/adminpage.jsp";
			}
			
			else
			{
				nextPage = "index.jsp?content=administrator/administrator.jsp";
			}
			*/
			//request.setAttribute("AdminList", AdminList); 
			
			// ������ �̵�. AdminList ����ϵ���... �׽�Ʈ��
			//nextPage = cs.XPathLogin(dir, id, password);
			return nextPage;
	}
	
//	@RequestMapping(value="admin", params="action=backup")
//	public String backup(String cmd, HttpServletRequest request) throws Exception {
//	
//		String result = null;
//		
//		result = cs.CmdExcuter(cmd);
//		
//		request.setAttribute("result", result);
//		
//		return "index.jsp?content=administrator/adminpage";
//	}

	
}

package kr.co.trinity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	@RequestMapping(value="/home", method = RequestMethod.GET)
	public String home(String userId){
		if(userId ==null || userId.equals(""))
		{
			return "index.jsp?content=login";
		}else{
			return "redirect:board?action=boardList";
		}
	}
	
}

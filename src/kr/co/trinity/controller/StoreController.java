package kr.co.trinity.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xml.sax.SAXException;

import kr.co.trinity.service.StoreXMLLogic;
import kr.co.trinity.service.XPathParser;
import kr.co.trinity.vo.Product;

@Controller
public class StoreController{
	StoreXMLLogic xml = new StoreXMLLogic();
	ArrayList<Product> prodList = new ArrayList<Product>();
	
	@RequestMapping(value="/store", params="action=home" )
	public String list(String flag, String keyWord, HttpServletRequest request){
		// TODO Auto-generated method stub
		String dir = request.getSession().getServletContext().getRealPath("xml/data.xml");
		try {
			prodList = xml.getXML(dir);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(prodList.toString());
		request.setAttribute("prodList", prodList);
		
		return "index.jsp?content=store/store";
	}

	@RequestMapping(value="/store", method= RequestMethod.GET )
	public String search(String value, String keyWord, HttpServletRequest request)
	{
		XPathParser xPath = new XPathParser();
	
		String dir = request.getSession().getServletContext().getRealPath("xml/data.xml");
		
		try {
			prodList = xPath.excute(dir, keyWord, value);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(prodList.isEmpty())
		{
			request.setAttribute("prodList", prodList);
			request.setAttribute("message", keyWord + " 의 검색결과가 없습니다.");
		}
		
		else
			request.setAttribute("prodList", prodList);
		return "index.jsp?content=store/store";
	}
}

package kr.co.trinity.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.co.trinity.service.CustomerServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController  {
	@Autowired
	CustomerServiceImpl cs ;
	
	@RequestMapping(value="/customer", params="action=login" )
	public String login(String userId, String password,
			            HttpServletResponse response, HttpSession session)
			                  throws Exception {
		String nextPage=null;
		String role=null;
		String userNum =null;
					
		boolean loginSuccess=false;
		boolean adminCheck=false;

		
		loginSuccess = cs.loginCheck(userId,password);
		adminCheck = cs.adminCheck(userId, password);
		userNum = cs.getUserNum(userId);

		
		role=adminCheck?"admin":"user";
		
		//view select
		if(loginSuccess){
			Cookie userIdCookie = new Cookie("userId", userId);
			Cookie passwordCookie = new Cookie("password", password);
			Cookie roleCookie = new Cookie("role", role);
			Cookie userNumCookie = new Cookie("userNum", String.valueOf(userNum));
			
			userIdCookie.setMaxAge(60*60);
			passwordCookie.setMaxAge(60*60);
			roleCookie.setMaxAge(60*60);
			userNumCookie.setMaxAge(60*60);
			
			//Cookie setting
			response.addCookie(userIdCookie);
			response.addCookie(passwordCookie);
			response.addCookie(roleCookie);
			response.addCookie(userNumCookie);

			session.setAttribute("userId",userId);
			session.setAttribute("password", password);
		    session.setAttribute("role", role);      	
		    session.setAttribute("userNum", userNum);
	
		    
			nextPage="redirect:board?action=boardList";
		}else{
			nextPage="index.jsp?content=login";
		}
		
		return nextPage;
		
	}
	
	@RequestMapping(value="/customer", params="action=logout")
	public String logout(HttpSession session) {
		if(session != null ) session.invalidate();
	
		return "index.jsp?content=login";
	}

}

package kr.co.trinity.exception;

public class RecordNotFoundException extends Exception {

	public RecordNotFoundException() {
		super("에러");
	}

	public RecordNotFoundException(String message) {
		super("에러 : "+message);
	}
	public RecordNotFoundException(int userNum) {
		super("userNum 에러 : "+userNum);
	}
}

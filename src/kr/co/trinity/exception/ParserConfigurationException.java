package kr.co.trinity.exception;

public class ParserConfigurationException extends Exception {

	public ParserConfigurationException() {
		super("xml 파싱에러");
	}

	public ParserConfigurationException(String message) {
		super("xml 파싱에러 : "+ message);
	}
}

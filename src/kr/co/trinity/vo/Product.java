package kr.co.trinity.vo;

public class Product {
	private String 	name;
	private int 	price;
	private String	descr;
	
	public Product() {
		super();
	}
	
	public Product(String name, int price, String descr) {
		this.name = name;
		this.price = price;
		this.descr = descr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + ", descr="
				+ descr + "]";
	}
	
}

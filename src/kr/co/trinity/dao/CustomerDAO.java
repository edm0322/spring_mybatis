package kr.co.trinity.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.trinity.exception.DulplicateException;
import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.vo.Customer;

@Repository
public class CustomerDAO implements CustomerDAOIF {
	@Autowired
	SqlSession session;
	
	private boolean isExist(String userId){
		boolean flag=false;
		
		String user = session.selectOne("CustomerMapper.isExist",userId);
		if(user != null) flag=true;
		
		return flag;
	}
	@Override
	public void insert(Customer customer) throws DulplicateException {
		   if(isExist(customer.getUserId())) 
			     throw new DulplicateException(customer.getUserId());
		   
		   session.insert("CustomerMapper.insert", customer);
	
	}

	@Override
	public void update(Customer customer) throws RecordNotFoundException {
		   if(!isExist(customer.getUserId())) 
			     throw new RecordNotFoundException(customer.getUserId());
		   session.update("CustomerMapper.update", customer);
	}

	@Override
	public void delete(String userId) throws RecordNotFoundException {
		   if(!isExist(userId)) 
			     throw new RecordNotFoundException(userId);

		   session.delete("CustomerMapper.delete",userId);
	}

	@Override
	public Customer getCustomer(String userNum) throws RecordNotFoundException {
		Connection conn=null;
		PreparedStatement stmt=null;
		ResultSet result=null;
		String sql="SELECT * FROM customer WHERE user_num=?";
		Customer customer=null;
		
	    customer = session.selectOne("getCustomer",userNum);
	    if(customer == null)
				throw new RecordNotFoundException(userNum);
	
		return customer;
	}

	@Override
	public ArrayList<Customer> getAllCustomer() {
	
		return (ArrayList)session.selectList("getAllCustomer");
	}
	
	@Override
	public boolean loginCheck(String userId, String password) throws Exception{
		boolean flag=false;
		HashMap<String, String> param=new HashMap<String, String>();
		param.put("userId", userId);
		param.put("password", password);
		
		String user = session.selectOne("loginCheck",param);
		
		if(user != null) flag=true;
		
		return flag;
	}
	
	@Override
	public boolean loginCheck(String userId) throws Exception{
		boolean flag=false;
		String user = session.selectOne("idCheck", userId);
		if(user != null) flag=true;
			
		return flag;
	}
	@Override
	public boolean adminCheck(String userId, String password) throws Exception {
		boolean flag = false;
		
		HashMap<String, String> param=new HashMap<String, String>();
		param.put("userId", userId);
		param.put("password", password);		
	    String user=session.selectOne("adminCheck", param);
		if(user != null) flag = true;
		
		return flag;
	}

	@Override
	public String getUserNum(String userId) throws RecordNotFoundException{
		String user = null;
		user = session.selectOne("getUserNum", userId);
		if(user == null) throw new RecordNotFoundException();
		return user;
	}
}













package kr.co.trinity.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import kr.co.trinity.exception.RecordNotFoundException;
import kr.co.trinity.vo.Board;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BoardDAO implements BoardDAOIF {

	@Autowired
	SqlSession session;
	
	private boolean isExist(int boardNum){
		boolean exist=false;
		int result=session.selectOne("BoardMapper.isExist",boardNum);
		if(result > 0) exist=true;
		return exist;
	}

	@Override
	public void insert(Board b) {
	    session.insert("BoardMapper.insert", b);
	}

	@Override
	public void update(Board b) throws RecordNotFoundException {
		if(!isExist(b.getBoardNum())) 
			     throw new RecordNotFoundException(""+b.getBoardNum());
		session.update("BoardMapper.update", b);
	}

	@Override
	public void delete(int boardNum) throws RecordNotFoundException {
		   if(!isExist(boardNum)) 
			     throw new RecordNotFoundException(new Integer(boardNum).toString());

		   session.delete("BoardMapper.delete", boardNum);
	}

	@Override
	public Board getBoard(int boardNum) throws RecordNotFoundException {
		Board board=null;
		
		board = session.selectOne("getBoard", boardNum);
		
		if(board == null ) throw new RecordNotFoundException(""+boardNum);
		return board;
	}

	@Override
	public ArrayList<Board> getAllBoard() {
	
		return (ArrayList)session.selectList("getAllBoard");
	}

	@Override
	public ArrayList<Board> getAllBoard(int pageSize, int pageNumber) {
	
	
		HashMap<String, Integer> param=new HashMap<String, Integer>();
		param.put("startNum",pageNumber * pageSize - 4 );
		param.put("endNum",pageNumber * pageSize );
			
		return (ArrayList)session.selectList("pageList", param);
	}
	
	@Override
	public ArrayList<Board> getBoardFindByTitle(String title) {
		return (ArrayList)session.selectList("getBoardFindByTitle",title);
	}
	
	@Override
	public ArrayList<Board> getBoardFindByWriter(String writer) {
		return (ArrayList)session.selectList("getBoardFindByWriter",writer);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public ArrayList<Board> getBoardFindByWriter(int pageSize, int pageNumber, String writer) {
		
		HashMap<String, Object> param=new HashMap<String, Object>();
		param.put("startNum",pageNumber * pageSize - 4 );
		param.put("endNum",pageNumber * pageSize );
		param.put("writer",writer);
			
		return (ArrayList)session.selectList("pageListByWriter", param);
	}
	public int getTotalCount(){
		
		int cnt = 0;
		cnt=session.selectOne("getTotalCount");
		
		return cnt ;
	}
}

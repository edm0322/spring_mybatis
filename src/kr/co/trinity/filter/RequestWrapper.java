package kr.co.trinity.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.owasp.esapi.ESAPI;

public class RequestWrapper extends HttpServletRequestWrapper {

	public RequestWrapper(HttpServletRequest request) {
		super(request);
		// TODO Auto-generated constructor stub
	}

	public String[] getParameterValues(String param){
		String[] values = super.getParameterValues(param);
		if(values == null) return null;
		int count = values.length;
		String[] encodedValues = new String[count];
		for(int i=0; i < count; i++){
			encodedValues[i]=ESAPI.encoder().encodeForHTML(values[i]);
		}
	 return encodedValues;
	}
	
	public String getParameter(String param){
		 String value = super.getParameter(param);
		 if(value==null) return null;
	 return ESAPI.encoder().encodeForHTML(value);
	}
	
	public String getHeader(String name){
		 String value = super.getHeader(name);
		 if( value == null) return null;
		 return ESAPI.encoder().encodeForHTML(value);
	}
}

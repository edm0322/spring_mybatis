<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="kr.co.trinity.vo.Customer" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>정보수정</title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 <script>
 function check(){
	 var form = document.form1;
	 
	 //신규 비밀번호 검사
	 if(form.newPassword.value != form.newPasswordConfirm.value)
		 {
		  alert("신규 비밀번호와 비밀번호 확인이 다릅니다.");
          form.newPassword.focus();//포커스를 신규 비밀번호 박스로 이동.
          return;
		 }
	 
	 form.submit();
 }
 
 </script>
  
 <style type="text/css">
	#customer_wrap{width:99%; padding-top:2%;}	
	.customer_table{width:40%; margin:0 auto;}	
 </style>
 <meta charset="utf-8">
</head>

<body>
<div id="customer_wrap">
<h2 style="text-align:center; padding-bottom:20px; color : #111111;">정보수정</h2>
<form action="customer?action=update" method="post" name="form1">
<input type="hidden" class = "form-control" name="userId" value="${requestScope.customer.getUserId()}" />
<input type="hidden" class = "form-control" name="userNum" value="${requestScope.customer.getUserNum()}" />

	<table class="table table-bordered customer_table" border="1">
		<colgroup>
			<col width="20%"/>
			<col width="80%"/>
		</colgroup>
		
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">이&nbsp;&nbsp;&nbsp;름</td>
		     <td>
		       	<input type="text" class = "form-control" name="userName" value="${requestScope.customer.getUserName()}" readonly/>
		     </td>
		</tr>
		
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">나&nbsp;&nbsp;&nbsp;이</td>
		     <td>
		       	<input type="text" class = "form-control" name="age" value="${requestScope.customer.getAge()}"/>
		     </td>
		</tr>
		
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">이메일</td>
		     <td>
		       	<input type="text" class = "form-control" name="email" value="${requestScope.customer.getEmail()}"/>
		     </td>
		</tr>

		<tr>
		     <td style="background:#f2f2f2; text-align:center;">신규 비밀번호</td>
		     <td>
		       	<input type="text"  class = "form-control"name="newPassword"/>
		     </td>
		</tr>
		
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">비밀번호 확인</td>
		     <td>
		       	<input type="text"  class = "form-control"name="newPasswordConfirm"/>
		     </td>
		</tr>
		
		<tr style="text-align:center;">
		     <td colspan="2">
		     	<input type="button" class = "btn btn-success btn-sm" value="확인" onclick="check();"/>&nbsp;&nbsp;
		     	<input type="reset"  class = "btn btn-warning btn-sm" value="취소"/>
		     </td>
		</tr>
	</table>
</form> 
</div><!-- customer_wrap -->

</body>
</html>
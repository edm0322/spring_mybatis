<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>회원가입</title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  
 <style type="text/css">
	#customer_wrap{width:99%; padding-top:2%;}	
	.customer_table{width:40%; margin:0 auto;}	
 </style>
</head>
<body>
<div id="customer_wrap">
<h2 style="text-align:center; padding-bottom:20px; color : #111111;">회원가입</h2>
<form action="customer?action=save" method="post">
	<table class="table table-bordered customer_table" border="1">
		<colgroup>
			<col width="20%"/>
			<col width="80%"/>
		</colgroup>
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">아이디</td>
		     <td>
		     	<input type="text" class = "form-control" name="userId"/>
		     </td>
		</tr>
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">암&nbsp;&nbsp;&nbsp;호</td>
		     <td>
		       	<input type="password"  class = "form-control"name="password"/>
		     </td>
		</tr>
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">이&nbsp;&nbsp;&nbsp;름</td>
		     <td>
		       	<input type="text" class = "form-control" name="userName"/>
		     </td>
		</tr>
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">나&nbsp;&nbsp;&nbsp;이</td>
		     <td>
		       	<input type="text" class = "form-control" name="age"/>
		     </td>
		</tr>
		<tr>
		     <td style="background:#f2f2f2; text-align:center;">이메일</td>
		     <td>
		       	<input type="text" class = "form-control" name="email"/>
		     </td>
		</tr>
		<tr style="text-align:center;">
		     <td colspan="2">
		     	<input type="submit" class = "btn btn-success btn-sm" value="가입"/>&nbsp;&nbsp;
		     	<input type="reset"  class = "btn btn-warning btn-sm" value="취소"/>
		     </td>
		</tr>
	</table>
</form> 
</div><!-- customer_wrap -->

</body>
</html>
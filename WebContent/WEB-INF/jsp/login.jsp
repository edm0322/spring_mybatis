<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Page</title>
<script>
	function register(){
		document.loginForm.action="customer?action=input";
		document.loginForm.submit();
	}
</script>

  
  <style type="text/css">
	 body{background:#f6f6f9;}
	 .container{padding:10% 20% 0 20%;}
	 .logo{text-align:center;}
	 .table{background:#ffffff; 
	 		border:1px solid #c3c3c3;}
  </style>
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body> 
		<div style = "padding:30px 0 100px 0;margin:0 auto; width:721px;">
		<form name="loginForm" action="customer?action=login" method="post">
			<div class = "login_form01">
				<div class = "input_id">
					<input type = "text" name = "userId" class="input">
				</div>
				<div class = "input_password">
					<input type = "password" name = "password" class="input">
				</div>
				<div class = "btn">
					<span class = "login_form_submit">
					<input type = "image" src = "images/login-btn.jpg"></span>
				</div>
				<div class = "btn2">
					<span class = "login_form_submit">
					<input type = "image" src = "images/btn_login02.gif" onclick="register()"></span>
				</div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</div>
		</form>
		</div>
		<!-- id : admin , passwd : 1q2w3e -->
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<form action="board?action=boardViewSecret&boardNum=${boardNum}" method="post">
	<%
		Object boardNum = request.getAttribute("boardNum");
	%>
	<table id="inputPass" border="0" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>
					비밀번호를 입력해주세요.
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<input type="password" name="password" value="">
					<input type="submit" value="확인">
				</td>
			</tr>
		</tbody>
	</table>
</form>
</body>
</html>
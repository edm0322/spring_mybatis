<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div id="sub_wrap">
    

        <!--컨테이너 시작-->
        <div id="container">
        
        	
            
            <div class="space64"></div>
        
        	<!--컨텐츠 시작-->
            <div class="sub_content">
            
            	<!--왼쪽 메뉴 시작-->
            	<div class="sub_content_left">
                
                	<div class="sc_left_top">
                            
                        <div class="sc_left_top_tit">
                        	<img src="images/company/left_top_tit.gif" />
                        </div>    
                                        
                    </div>
                    
                    <div class="space16"></div>

					<div class="sc_left_menu">
                    
                    	<ul>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=1" class="rollover"><img src="images/company/left_menu01.gif" /><img src="images/company/left_menu01_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=2"><img src="images/company/left_menu02_ov.gif" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=3" class="rollover"><img src="images/company/left_menu03.gif" /><img src="images/company/left_menu03_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=4" class="rollover"><img src="images/company/left_menu04.gif" /><img src="images/company/left_menu04_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=5" class="rollover"><img src="images/company/left_menu05.gif" /><img src="images/company/left_menu05_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        </ul>
                    
                    </div>
                    
                    <div class="space48"></div>
                
                </div>
                <!--왼쪽 메뉴 종료-->
                
                <!--오른쪽 내용 시작-->
                <div class="sub_content_right">
                
                	<!--상단 타이틀 시작-->
                	<div class="sc_right_top">
                    
                    	<div class="sc_right_top_tit">
                        	<img src="images/company/right_top_tit02.gif" />
                        </div>
                    
                    </div>
                	<!--상단 타이틀 종료-->
                
                	<div class="space07"></div>
                
                	<!--내용 시작-->
                	<div class="sc_right_body">
                    
                   	    <div class="sc_right_body">
                    	<img src="images/company/company02_text01.gif" />
                    	</div>
                    
                		<div class="space39"></div>
                		
                		<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/141.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                웹방화벽 '웹스레이' / 시큐어코딩솔루션 '코드레이' 라인업<br/>
벤처기업 재인증 (연구개발기업)<br/>
인사이트프로 웹방화벽 국내총판 계약 체결<br/>
'코드레이 V2.0' 조달청 나라장터 등록완료<br/>
'웹 서버의 보안장치'에 관한 특허획득 [특허 제 10-1147251호]<br/>
트리니티소프트, 2012 솔루션데이 개최<br/>
2012 신기술 실용화 촉진대회 지식경제부장관 표창
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>
                        
                        <div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img00.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                코드레이 XG V2.5, CC인증계약 체결<br/>
eGISEC FAIR 2014 참가 : "적용사례로 보는 취약점점검도구의 선택기준"강연<br/> 
Codegate 2014 참가<br/>
AppSec 2014 참가 : "시큐어코딩 솔루션의 선택기준"강연 <br/>
코드레이 XG V2.5, 국정원으로부터 CC인증 획득<br/>
코드레이, 전자신문 2014 상반기 인기상품 선정<br/>
2014 트리니티소프트 비즈니스 파트너데이 개최<br/>
ISEC 2014 참가 : "SW오류검증을 위한 시큐어코딩 진단도구"강연
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>
                        
                        <div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img000.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                AppSec 2013 참가 : "시큐어코딩 법제화에 따른 대응방안"강연<br />
                                eGISEC FAIR 2013 참가 : "시큐어코딩기법을 적용하지 않은 취약한 코딩사례 및 대책"강연<br />
                                2013 트리니티소프트 비즈니스 파트너데이 개최<br />
                                제 11회 국방정보보호 컨퍼런스 참가<br />
                                2013 정보보호 제품전시회 참가<br />
                                본사 사옥이전(구로디지털단지 -> 경기 안양시 동안구 관양동)<br />
                                제 7회 금융보안 & 개인정보보호 컨퍼런스 참가<br />
                                PASCON 2013참가<br />
                                ISEC 2013 참가 : "취약점 탐지 극대화를 위한 시큐어코딩 점검도구의 적용사례는?" 강연
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>
                    
                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img01.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                웹방화벽 '웹스레이' / 시큐어코딩솔루션 '코드레이' 라인업<br />
                                벤처기업 재인증 (연구개발기업)<br />
                                인사이트프로 웹방화벽 국내총판 계약 체결<br />
                                '코드레이 V2.0' 조달청 나라장터 등록완료<br />
                                '웹 서버의 보안장치'에 관한 특허획득 [특허 제 10-1147251호]<br />
                                트리니티소프트, 2012 솔루션데이 개최<br />
                                2012 신기술 실용화 촉진대회 지식경제부장관 표창
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img02.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                웹방화벽에서의 화이트 유알엘 수집방법 및 화이트 유알엘<br />
                                수집기능이 구비된 웹방화벽'에 관한 특허획득 [특허 제 10-1024006-0000]<br />
                                코드레이 CC인증계약 체결<br />
                                트리니티 소프트, 2011 파트너 세미나 개최<br />
                                '코드레이 GS인증계약 체결'<br />
                                '코드레이 V2.0' 국가정보원 CC인증 획득(EAL2)<br />
                                '코드레이 V2.0' 한국정보통신기술협회(TTA)GS인증 획득<br />
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img03.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                벤처기업 재인증 (연구개발기업)<br />
                                중소기업청 산업보안과제 수행 기업으로 선정<br />
                                QuEST포럼 품질경영시스템 TL9000 재인증 획득<br />
                                웹 소스코드 통합보안관리 솔루션 코드레이 V1.5.3 출시<br />
                                G20 서울 국제 정상회의 웹방화벽 대응 지원 장비 선정<br />
                                아세아텍 그룹 계열사 편입
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img04.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                '웹스레이' 국정원 암호화 모듈 탑재 완료<br />
                                '웹스레이' 행정안전부 행정정보보호용시스템 선정<br />
                                직접생산 증명서(전산업무개발) 획득<br />
                                광주정부통합전산센터 웹 방화벽 확충사업 공급자 선정<br />
                                2009 벤처코리아 벤처기업대상 중소기업청장상 수상<br />
                                QuEST포럼 품질경영시스템 TL9000인증 획득<br />
                                정부통합전산센터 5차 통합자원 사업 웹 방화벽 공급자 선정
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img05.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                \정부통합전산센터 정보보호 보강 사업 웹 방화벽 공급자 선정<br />
                                '네트워크 기반의 게시물 검증방법 및 그 장치'에 관한 특허 획득[특허 제 10-0917848호]<br />
                                컴퓨터프로그램보호위원회 'Webs-Ray V2.0' 프로그램 등록.<br />
                                '웹서버의 업로드 파일의 검증 방법 및 그 장치'에 관한 특허 획득[특허 제 10-0824789호]<br />
                                '웹스레이' 조달청 제3자단가계약 체결<br />
                                기술혁신형 중소기업(INNO-BIZ)확인서 획득<br />
                                지티플러스 웹 애플리케이션 방화벽 국내총판 계약 체결<br />
                                웹스레이 V2.5 CCRA 국제 CC인증 획득 (KISA EAL4)
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img06.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                국가정보원 개발시스템 환경 실사 통과<br />
                                (주)에스넷 시스템 국내총판 계약 체결<br />
                                웹 애플리케이션 방화벽 부문 세계최초 CCRA 국제 CC 인증 평가 완료(KISA EAL4)<br />
                                웹 보안 방법 및 그 장치에 관한 특허 획득 [특허 제 10-0732689호]<br />
                                웹 애플리케이션 방화벽 국내 최초 CCRA 국제 CC 인증 획득<br />
                                웹 애플리케이션 방화벽 국가정보원 보안적합성 검증필 획득<br />
                                웹 애플리케이션 방화벽 국가정보원 CC 인증 효력유지 및 변경승인 통과<br />
                                제 8회 대한 민국 소프트웨어 대전 'Webs-Ray' 정보통신부 장관상 수상<br />
                                벤처기업 재 인증 (연구개발기업: 기술신용보증기금)
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img07.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                국내외 최초 웹 애플리케이션 방화벽 CCRA 국제 CC 평가계약체결 (EAL4)<br />
                                웹스레이 V2.0 출시<br />
                                CCRA 국제 CC 인증 평가착수 (KISA & 국가정보원)<br />
                                정보통신부(TTA) Good Software 인증 획득 (Windows, Linux, Unix)<br />
                                중소기업청 제품 성능인증서 획득
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space30"></div>

                    	<div class="sc_right_body">
                        
                        	<div class="sc_body_left">
                            	<img src="images/company/company02_img08.gif" />
                            </div>

                        	<div class="sc_body_right gray03 font03 dotum">
                            
                                (주)트리니티 소프트 설립/기업부설연구소 설립<br />
                                웹 애플리케이션 보안 제품 '웹스레이' 상표 등록<br />
                                '웹스레이' 베타 버전 출시<br />
                                한국산업기술 진흥협회 기업부설연구소 인증 획득<br />
                                프로그램심의조정위원회 '웹스레이 1.5.3'프로그램 등록<br />
                                웹 애플리케이션 보안 서비스 런칭<br />
                                벤처기업인증 (신기술기업: 중소기업청)
                            
                            </div>
                        
                        	<div style="clear:both"></div>
                        
                        </div>
                        
                        <div class="space150"></div>
                    
                    </div>
                	<!--내용 종료-->
                
                </div>
            	<!--오른쪽 내용 종료-->
            
            	<div style="clear:both"></div>
            
            </div>
            <!--컨텐츠 종료-->
        
        </div>
        <!--컨테이너 종료-->
    </div>

</body>
</html>

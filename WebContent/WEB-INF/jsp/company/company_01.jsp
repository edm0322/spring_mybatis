<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="sub_wrap">
    
        <!--컨테이너 시작-->
        <div id="container">

            <div class="space64"></div>
        
        	<!--컨텐츠 시작-->
            <div class="sub_content">
            
            	<!--왼쪽 메뉴 시작-->
            	<div class="sub_content_left">
                
                	<div class="sc_left_top">
                            
                        <div class="sc_left_top_tit">
                        	<img src="images/company/left_top_tit.gif" />
                        </div>    
                                        
                    </div>
                    
                    <div class="space16"></div>

					<div class="sc_left_menu">
                    
                    	<ul>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=1" class="rollover">
                                		<img src="images/company/left_menu01.gif" />
                                		<img src="images/company/left_menu01_ov.gif" class="over"/>
                                	</a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=2" class="rollover">
                                		<img src="images/company/left_menu02.gif" />
                                		<img src="images/company/left_menu02_ov.gif" class="over"/>
                                	</a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=3" class="rollover">
                                		<img src="images/company/left_menu03.gif" />
                                		<img src="images/company/left_menu03_ov.gif" class="over"/>
                                	</a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=4" class="rollover">
                                		<img src="images/company/left_menu04.gif" />
                                		<img src="images/company/left_menu04_ov.gif" class="over"/>
                                	</a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=5" class="rollover">
                                		<img src="images/company/left_menu05.gif" />
                                		<img src="images/company/left_menu05_ov.gif" class="over"/>
                                	</a>
                                </div>
                            </li>
                        </ul>
                    
                    </div>
                    
                    <div class="space48"></div>
                
                </div>
                <!--왼쪽 메뉴 종료-->
                
                <!--오른쪽 내용 시작-->
                <div class="sub_content_right">
                
                	<!--상단 타이틀 시작-->
                	<div class="sc_right_top">
                    
                    	<div class="sc_right_top_tit">
                        	<img src="images/company/right_top_tit01.gif" />
                        </div>
                    
                    </div>
                	<!--상단 타이틀 종료-->
                
                	<!--내용 시작-->
                	<div class="sc_right_body">
                    
                    	<div class="sc_right_box">
                        	
                            <div class="sc_right_box_tit">
                            	<img src="images/company/right_title01.gif" />
                            </div>
                            
                        </div>
                        
                        <div class="sc_right_body">
                        	<img src="images/company/company01_img01.gif" />
                        </div>
                    
                    	<div class="space48"></div>
                        
                    	<div class="sc_right_box">
                        	
                            <div class="sc_right_box_tit">
                            	<img src="images/company/right_title02.gif" />
                            </div>
                            
                        </div>
                        
                        <div class="sc_right_body">
                        	<img src="images/company/company01_img02.gif" />
                        </div>
                    
                    	<div class="space48"></div>
                    
                    	<div class="sc_right_box01">
                        
                        	<div class="sc_right_box01_tit">
                            	<img src="images/company/right_title03.gif" />
                            </div>
                        
                        </div>
                        
                        <div class="space19"></div>
                        
                        <div class="sc_right_body01 gray02 dotum">
                        
                            <strong>버스</strong><br />
                            - 초록버스(지선) <img src="images/company/bus_g.gif"></img> : 5번, 5-1번, 5602번, 5531번, 5534번, 5616번, 5620번, 5623번,<br />
                                               <span class="padding105">5625번, 5633번, 5713번, 6512번</span> <br />
                            - 파랑버스(좌석) <img src="images/company/bus_b.gif"></img> : 541번<br />
                            - 빨강버스(광역) <img src="images/company/bus_r.gif"></img> : 3030번, 1303번, 9503번
                        
                  			<div class="space42"></div>      
                        
                            <strong>지하철</strong><br />
                            - 인덕원역(4호선) 7번 출구 -> 직진 -> 인덕원성당 옆골목 -> 직진 -> 관양교 -> 금강펜테리움IT타워<br />
                            - 7분 정도 소요
                        
                        </div>
                        
                        <div class="space150"></div>
                    
                    </div>
                	<!--내용 종료-->
                
                </div>
            	<!--오른쪽 내용 종료-->
            
            	<div style="clear:both"></div>
            
            </div>
            <!--컨텐츠 종료-->
        
        </div>
        <!--컨테이너 종료-->
        </div>
</body>
</html>

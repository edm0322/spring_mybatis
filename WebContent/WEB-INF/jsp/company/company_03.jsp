<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div id="sub_wrap">
    
        <!--컨테이너 시작-->
        <div id="container">
        
        	
            
            <div class="space64"></div>
        
        	<!--컨텐츠 시작-->
            <div class="sub_content">
            
            	<!--왼쪽 메뉴 시작-->
            	<div class="sub_content_left">
                
                	<div class="sc_left_top">
                            
                        <div class="sc_left_top_tit">
                        	<img src="images/company/left_top_tit.gif" />
                        </div>    
                                        
                    </div>
                    
                    <div class="space16"></div>

					<div class="sc_left_menu">
                    
                    	<ul>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=1" class="rollover"><img src="images/company/left_menu01.gif" /><img src="images/company/left_menu01_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=2" class="rollover"><img src="images/company/left_menu02.gif" /><img src="images/company/left_menu02_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=3"><img src="images/company/left_menu03_ov.gif" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=4" class="rollover"><img src="images/company/left_menu04.gif" /><img src="images/company/left_menu04_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=company&number=5" class="rollover"><img src="images/company/left_menu05.gif" /><img src="images/company/left_menu05_ov.gif" class="over" /></a>
                                </div>
                            </li>
                        </ul>
                    
                    </div>
                    
                    <div class="space48"></div>
                
                </div>
                <!--왼쪽 메뉴 종료-->
                
                <!--오른쪽 내용 시작-->
                <div class="sub_content_right">
                
                	<!--상단 타이틀 시작-->
                	<div class="sc_right_top">
                    
                    	<div class="sc_right_top_tit">
                        	<img src="images/company/right_top_tit03.gif" />
                        </div>
                    
                    </div>
                	<!--상단 타이틀 종료-->
                
                	<div class="space07"></div>
                
                	<!--내용 시작-->
                	<div class="sc_right_body">
                    
                    	<div class="sc_right_box">
                        	<div class = "company_body">
                        		<ul>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img01.gif">
                        					<img src = "images/company/certificate01_img01_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img02.gif">
                        					<img src = "images/company/certificate01_img02_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img03.gif">
                        					<img src = "images/company/certificate01_img03_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img04.gif">
                        					<img src = "images/company/certificate01_img04_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar01">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img05.gif">
                        					<img src = "images/company/certificate01_img05_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar01">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img06.gif">
                        					<img src = "images/company/certificate01_img06_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar02">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate01_img07.gif">
                        					<img src = "images/company/certificate01_img07_ov.gif" class="over">
                        				</a>
                        			</li>
                        		</ul>
                        	</div>
                        	</div>
                        <div style="clear:both"></div>
                        <div class="space19"></div>    
                        <div class="sc_right_body">
                        	<div class="sc_right_box">
                        		<div class="sc_right_box_tit">
                        			<img src="images/company/company03_tit02.gif">
                        		</div>
                        	</div>
                        	<div class="company_body">
                        		<ul>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate02_img01.gif">
                        					<img src = "images/company/certificate02_img01_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate02_img02.gif">
                        					<img src = "images/company/certificate02_img02_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate02_img03.gif">
                        					<img src = "images/company/certificate02_img03_ov.gif" class="over">
                        				</a>
                        			</li>
                        		</ul>
                        	</div>
                        </div>
                        <div style="clear:both"></div>
                        <div class = "space19"></div>
                        	<div class="sc_right_body">
                        	<div class="sc_right_box">
                        		<div class="sc_right_box_tit">
                        			<img src="images/company/company03_tit03.gif">
                        		</div>
                        	</div>
                        	<div class="company_body">
                        		<ul>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate03_img01.gif">
                        					<img src = "images/company/certificate03_img01_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate03_img02.gif">
                        					<img src = "images/company/certificate03_img02_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li>
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate03_img03.gif">
                        					<img src = "images/company/certificate03_img03_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate03_img04.gif">
                        					<img src = "images/company/certificate03_img04_ov.gif" class="over">
                        				</a>
                        			</li>
                        			<li class="bar02">
                        				<a href = "#" class="rollover">
                        					<img src = "images/company/certificate03_img05.gif">
                        					<img src = "images/company/certificate03_img05_ov.gif" class="over">
                        				</a>
                        			</li>
                        		</ul>
                        	</div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="space150"></div>
                    
                    </div>
                	<!--내용 종료-->
                
         
            	<!--오른쪽 내용 종료-->
            
            	<div style="clear:both"></div>
            
            </div>
            <!--컨텐츠 종료-->
        
        </div>
        <!--컨테이너 종료-->
        
    </div>
</div>
</body>
</html>

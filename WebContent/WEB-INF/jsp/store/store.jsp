<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
<link href="css/pricing.css" rel="stylesheet" type="text/css" />

<title>Insert title here</title>
</head>

<body>
	<c:set var="products" value="${requestScope.prodList}"/>
	<c:set var="message" value="${requestScope.message}"/>
	
	<form name="myForm" action="store" method ="get" style="text-align:center;">
				상품검색   
	            <input type="text" name="keyWord"/>
	            <input type="hidden" name="value" value="945231"/>
	            <input type="submit" value="검색" />
	</form>
	
	<div id="pricing-table" class="clear">
	<h6>${message}</h6>
	<c:forEach items="${requestScope.prodList}" var="products" end="${requestScope.prodList.size()}">
		<c:choose>
		<c:when test="${products.name eq 'Code-Ray Pro' }">
		 <div class="plan">
	        <h3>${products.name }<span> 이미지 </span></h3>
	        <a class="signup" href="">Add to Cart</a>         
	        <ul>
	            <li><b>${products.price }</b> 가격</li>
	            <li><b>${products.descr }</b> </li>
	        </ul> 
	    </div>
		</c:when>
		<c:otherwise>
	    <div class="plan">
	        <h3>${products.name }<span> 이미지 </span></h3>
	        <a class="signup" href="">Add to Cart</a>         
	        <ul>
	            <li><b>${products.price }</b> 가격</li>
	            <li><b>${products.descr }</b> </li>
	        </ul> 
	    </div>
	    </c:otherwise>
	    </c:choose>
	</c:forEach>
	</div>
</body>
</html>
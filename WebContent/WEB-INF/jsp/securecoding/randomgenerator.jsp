<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<% ArrayList<String> result = (ArrayList<String>)request.getAttribute("result");%>	
	<table style="font-size:150%;">
		<tr>
			<td>
				<form action="securecoding?action=randomgenerator" method="post">
					<input type = "submit" value = "Math.random_generator" name = "math" style="font-size:20px">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type = "submit" value = "Securerandom_generator" name = "secure" style="font-size:20px">
				</form>
			</td>
		</tr>
		<tr>
			<td style = "font-size:50px; font-color:red;">
				<br>
				생성결과
			</td>
		</tr>
		<tr>
			<td>
				<br>
				<%
				  if(result != null)
					out.println(result);
				  if(result == null)
					out.println("랜덤 값을 생성하세요.");
				%>
			</td>
		</tr>
	</table>
</body>
</html>
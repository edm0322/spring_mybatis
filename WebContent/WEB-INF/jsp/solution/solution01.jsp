<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div id="sub_wrap">
        <!--컨테이너 시작-->
        <div id="container">
        
   
            <div class="space64"></div>
        
        	<!--컨텐츠 시작-->
            <div class="sub_content">
            
            	<!--왼쪽 메뉴 시작-->
            	<div class="sub_content_left">
                
                	<div class="sc_left_top02">
                            
                        <div class="sc_left_top_tit">
                        	<img src="images/solution/left_top_tit.gif" />
                        </div>    
                                        
                    </div>
                    
                    <div class="space16"></div>

					<div class="sc_left_menu">
                    	<style type = "text/css">
                    		.popGnb {display:none;position:absolute; right:-224px; top: -1px; z-index:500}
                    		.popGnb ul {margin:0; padding:0;}
                    		.popGnb ul li {margin:0; padding:0; list-style:none; height:46px;}
                    	</style>
                    	<script type="text/javascript">
                    		$(function(){
                    			$('.onGnb').mouseenter(function(){
                    					$('.popGnb').show();
                    			})
                    			$('.onGnb').mouseleave(function(){
                    					$('.popGnb').hide();
                    			})
                    		})
                    	</script>
                    	<ul>
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=solution&number=1" class="rollover">
                                		<img src="images/solution/left_menu01.gif">
                                		<img src="images/solution/left_menu01_ov.gif" class="over">
                                	</a>
                                </div>
                            </li>
                        	<li class="onGnb">
                                <div class="sc_left_menu_text">
                                	<a href="company?action=solution&number=2" class="rollover">
                                		<img src="images/solution/newLeftTit01.gif">
                                		<img src="images/solution/newLeftTit01_ov.gif" class="over">
                                	</a>
                                </div>
                                <div class="popGnb" style="display: none;">
                                	<ul>
                                		<li>
                                			<a href="company?action=solution&number=21" class = "rollover">
                                				<img src="images/solution/newLeft01.gif">
                                				<img src="images/solution/newLeft01_ov.gif" class="over">
                                			</a>
                                		</li>
                                		<li>
                                			<a href="company?action=solution&number=22" class = "rollover">
                                				<img src="images/solution/newLeft02.gif">
                                				<img src="images/solution/newLeft02_ov.gif" class="over">
                                			</a>
                                		</li>
                                		<li>
                                			<a href="company?action=solution&number=23" class = "rollover">
                                				<img src="images/solution/newLeft03.gif">
                                				<img src="images/solution/newLeft03_ov.gif" class="over">
                                			</a>
                                		</li>
                                		<li>
                                			<a href="company?action=solution&number=24" class = "rollover">
                                				<img src="images/solution/newLeft04.gif">
                                				<img src="images/solution/newLeft04_ov.gif" class="over">
                                			</a>
                                		</li>
                                	</ul>
                                </div>
                            </li>
                        	
                        	<li>
                                <div class="sc_left_menu_text">
                                	<a href="company?action=solution&number=3" class="rollover">
                                		<img src="images/solution/left_menu05.gif" />
                                		<img src="images/solution/left_menu05_ov.gif" class="over" />
                                	</a>
                                </div>
                            </li>
                        </ul>
                    
                    </div>
                    
                    <div class="space48"></div>
                
                </div>
                <!--왼쪽 메뉴 종료-->
                
                <!--오른쪽 내용 시작-->
                <div class="sub_content_right">
                
                	<!--상단 타이틀 시작-->
                	<div class="sc_right_top">
                    
                    	<div class="sc_right_top_tit">
                        	<img src="images/solution/right_top_tit01.gif" />
                        </div>
                    
                    </div>
                	<!--상단 타이틀 종료-->
                
                	<!--내용 시작-->
                	<div class="sc_right_body">
						<div class = "space52"></div>
                        <div class="sc_right_body">
                        
                        	<img src="images/solution/websray_main_img01.jpg" />
                        	<img src="images/solution/websray_main_img02.jpg" />
                        	<img src="images/solution/websray_main_img03.jpg" />
                        	<img src="images/solution/websray_main_img04.jpg" />
                        	<img src="images/solution/websray_main_img05.jpg" />
                        	<img src="images/solution/websray_main_img06.jpg" />
                        	<img src="images/solution/websray_main_img07.jpg" />
                        	<img src="images/solution/websray_main_img08.jpg" />
                            <br>
                            <br>
                            <br>
                        </div>
                        
                        <div class="space150"></div>
                    
                    </div>
                	<!--내용 종료-->
                
                </div>
            	<!--오른쪽 내용 종료-->
            
            	<div style="clear:both"></div>
            
            </div>
            <!--컨텐츠 종료-->
        
        </div>
        <!--컨테이너 종료-->
    </div>

</body>
</html>

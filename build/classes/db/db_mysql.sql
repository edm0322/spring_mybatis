DROP TABLE customer;

CREATE TABLE customer (
 user_id VARCHAR(20) PRIMARY KEY,
 password VARCHAR(100) NOT NULL,
 user_name VARCHAR(20) NOT NULL,
 age INTEGER(3),
 email VARCHAR(50),
 role VARCHAR(10)	NOT NULL,
 user_num VARCHAR(20) NOT NULL
);


DROP TABLE board;

CREATE TABLE board(
 board_num INTEGER(10) PRIMARY KEY,
 title VARCHAR(50) NOT NULL,
 writer VARCHAR(20) NOT NULL,
 contents VARCHAR(500),
 file_name VARCHAR(20),
 write_date DATE,
 update_date DATE,
 password VARCHAR(100)
);

insert into customer values ('admin', 'password','admin',30,'admin@secure.com','admin','123456789');
insert into customer values('user1', 'password','user1',20,'user1@secure.com','user','223456789');
insert into customer values('user2', 'password','user2',20,'user2@secure.com','user','223456788');
insert into customer values('user3', 'password','user3',20,'user3@secure.com','user','223456787');

commit;
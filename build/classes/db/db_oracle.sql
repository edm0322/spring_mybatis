DROP TABLE customer;

CREATE TABLE customer (
 user_id VARCHAR2(20) PRIMARY KEY,
 password VARCHAR2(100) NOT NULL,
 user_name VARCHAR2(20) NOT NULL,
 age NUMBER(3),
 email VARCHAR2(50),
 role VARCHAR2(10)	NOT NULL,
 user_num VARCHAR2(20)	NOT NULL
);


DROP TABLE board;

CREATE TABLE board(
 board_num NUMBER(10) PRIMARY KEY,
 title VARCHAR2(50) NOT NULL,
 writer VARCHAR2(20) NOT NULL,
 contents VARCHAR2(500),
 file_name VARCHAR2(20),
 write_date DATE,
 update_date DATE,
 password VARCHAR2(100)
);

insert into customer values ('admin', 'password','admin',30,'admin@secure.com','admin','123456789');
insert into customer values('user1', 'password','user1',20,'user1@secure.com','user','223456789');
insert into customer values('user2', 'password','user2',20,'user2@secure.com','user','223456788');
insert into customer values('user3', 'password','user3',20,'user3@secure.com','user','223456787');

insert into board values('1', 'test', 'tester', 'tester', 'test', '3000/3/3', '3000/3/3', '');
insert into board values('2', 'test', 'tester', 'tester', 'test', '4000/4/4', '4000/4/4', '');
insert into board values('3', 'test', 'tester', 'tester', 'test', '5000/5/5', '5000/5/5', '');


commit;